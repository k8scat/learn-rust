/// 文档注释作为测试
///
/// Adds one to the number given.
///
/// # Examples
///
/// ```
/// let arg = 5;
/// let answer = learn_rust::writing_tests::writing_tests::add_one(arg);
///
/// assert_eq!(6, answer);
/// ```
pub fn add_one(x: i32) -> i32 {
    x + 1
}

pub fn add_two(a: i32) -> i32 {
    a + 2
}

fn internal_adder(a: i32, b: i32) -> i32 {
    a + b
}

/// 测试模块的 #[cfg(test)] 注解告诉 Rust 只在执行 cargo test 时才编译和运行测试代码，而在运行 cargo build 时不这么做。
/// 这在只希望构建库的时候可以节省编译时间，并且因为它们并没有包含测试，所以能减少编译产生的文件的大小。
/// 与之对应的集成测试因为位于另一个文件夹，所以它们并不需要 #[cfg(test)] 注解。
/// 然而单元测试位于与源码相同的文件中，所以你需要使用 #[cfg(test)] 来指定他们不应该被包含进编译结果中。
#[cfg(test)]
mod tests {
    use super::*;

    #[test] // 这个属性表明这是一个测试函数，这样测试执行者就知道将其作为测试处理
    fn it_works() {
        assert_eq!(2 + 2, add_two(2), "msg while err") // 函数体通过使用 assert_eq! 宏来断言 2 加 2 等于 4
    }

    /// should_panic 这个属性在函数中的代码 panic 时会通过，而在其中的代码没有 panic 时失败
    /// 为了使 should_panic 测试结果更精确，我们可以给 should_panic 属性增加一个可选的 expected 参数。
    /// should_panic 属性中 expected 参数提供的值是 panic 信息的子串
    #[test]
    #[should_panic(expected = "Make")]
    fn another() {
        panic!("Make this test fail");
    }

    #[test]
    fn it_works2() -> Result<(), String> {
        if 2 + 2 == 4 {
            Ok(())
        } else {
            Err(String::from("it_works2 err"))
        }
    }

    #[test]
    #[ignore] // 使用 ignore 属性来标记耗时的测试并排除他们
    fn expensive_test() {
        // 需要运行一个小时的代码
    }

    #[test]
    fn internal_test() {
        assert_eq!(4, internal_adder(2, 2));
    }
}

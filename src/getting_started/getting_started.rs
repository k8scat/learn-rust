fn hello_world() {
    println!("Hello, World!");
}

/// cargo test println_hello_world -- --nocapture
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn println_hello_world() {
        hello_world();
    }
}

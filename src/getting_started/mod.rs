//! 1. 入门指南
//!
//! 安装：`curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh`
//!
//! 另外，你还需要一个链接器（linker），这是 Rust 用来将其编译的输出连接到一个文件中的程序。
//! 在 macOS 上，你可以通过运行以下命令获得 C 语言编译器：
//! xcode-select --install
//!
//! 更新：rustup update
//!
//! 卸载：rustup self uninstall
//!
//! rustc 版本：rustc --version
//!
//! 直接编译运行：
//! rustc hello_world.rs
//! ./hello_world
//!
//!
//! Cargo 是 Rust 的构建系统和包管理器。
//! 大多数 Rustacean 们使用 Cargo 来管理他们的 Rust 项目，因为它可以为你处理很多任务，比如构建代码、下载依赖库并编译这些库。
//!
//! cargo 版本：cargo --version
//!
//! 创建二进制项目：cargo new hello-world
//! 创建库项目：cargo new hello-world-lib --lib
//!
//! 构建：cargo build
//! 运行：cargo run
//! 检查：cargo check
//! 发布构建：cargo build --release
pub mod getting_started;

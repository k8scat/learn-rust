/// 可以通过指定测试函数的名称作为 cargo test 的参数来运行特定集成测试。也可以使用 cargo test 的 --test 后跟文件的名称来运行某个特定集成测试文件中的所有测试：
/// cargo test --test integration_test
mod common;

use learn_rust::writing_tests::writing_tests;

#[test]
fn it_adds_two() {
    common::setup();
    assert_eq!(4, writing_tests::add_two(2));
}
